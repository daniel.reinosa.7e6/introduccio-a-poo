package MechanicalArmApp
class MechArm (){
    var ences = false
    var posicio = 0
    var heigh = 0

    init {
        ences = false
        posicio = 0
        heigh = 0
    }
    fun On(){
        if(ences) ences = false
        else ences = true
    }
    fun elevacio(altitude:Int){
        heigh += altitude
        if (heigh > 30) heigh -= 30

    }
    fun angle(angle:Int){
        posicio += angle
        if (posicio > 360) posicio -=360
    }

}
fun imputOrders(robot: MechArm){
    robot.On()
    printFinal(robot)
    robot.elevacio(3)
    printFinal(robot)
    robot.angle(180)
    printFinal(robot)
    robot.elevacio(-3)
    printFinal(robot)
    robot.angle(-180)
    printFinal(robot)
    robot.angle(3)
    printFinal(robot)
    robot.On()
    printFinal(robot)
}

fun printFinal(robot: MechArm){
    println("MechanicalArm{openAngle=${robot.posicio}, altitude=${robot.heigh}, turnedOn=${robot.ences}}")
}

fun main (){
    val brazo = MechArm()
    imputOrders(brazo)

}