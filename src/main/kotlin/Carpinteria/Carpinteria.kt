package Carpinteria

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Indica Taulell o LListons")
    val sc = scanner.next().uppercase()
    when(sc){

        "TAULELL" -> {
            println()
            println("Indica el preu dels Taulells")
            val preuLlistons = scanner.nextInt()
            println("Indica la mida del Taulell amplada x llargada")
            val midaAmpladaTaulell = scanner.nextInt()
            val midaLlargadaTaulell = scanner.nextInt()
            val metreQuadrat = midaAmpladaTaulell*midaLlargadaTaulell
            val preu = metreQuadrat*preuLlistons
            println("El preu es $preu €")
        }
        "LLISTONS" ->{
            println("Indica el preu dels llistons")
            val preuUnitariLlistons = scanner.nextInt()
            println("Indica la mida dels llistons")
            val midaLlistons = scanner.nextInt()
            val preu = midaLlistons*preuUnitariLlistons
            println("El preu es $preu €")
        }
    }
}