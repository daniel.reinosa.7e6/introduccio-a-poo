package LampadaAliBaba

val ANSI_RESET = "\u001B[0m"
val ANSI_BLACK = "\u001B[40m"
val ANSI_RED = "\u001B[41m"
val ANSI_GREEN = "\u001B[42m"
val ANSI_YELLOW = "\u001B[43m"
val ANSI_BLUE = "\u001B[44m"
val ANSI_PURPLE = "\u001B[45m"
val ANSI_CYAN = "\u001B[46m"
val ANSI_WHITE = "\u001B[47m"

val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN,ANSI_GREEN)

class Lampada(val id: Int){
    var color = 0
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    private var intensitatPujada=true
    private var offColor = 1
    var intensitat = 0

    fun encender(){
        if(color==0){
            color=offColor
            intensitat=1
            intensitatPujada=true
        }
        printLamp()
    }

    fun apagar(){
        if(color>0){
            offColor=color
            color=0
            intensitat=0
        }
        printLamp()
    }

    fun cambiarColor (){
        if (color!=colors.size-1 && color != 0) color++
        else if(color==colors.size-1 && color != 0) color=1
        printLamp()
    }

    fun cambiarIntensidad() {
        if (intensitatPujada && color != 0 && intensitat!=5) intensitat++
        else if (intensitatPujada && intensitat==5 ){
            intensitatPujada=false
            intensitat--
        }
        else if (!intensitatPujada && intensitat>1) intensitat--
        else if (!intensitatPujada && intensitat==1 && color != 0){
            intensitatPujada = true
            intensitat++
        }
        printLamp()
    }

    fun printLamp(){
        println("Lampada: ${this.id} | Color: ${colors[this.color]}    $ANSI_RESET - Intensitat ${this.intensitat}")
    }

}
//
//fun main(){
//    val lampada1 = Lampada(1)
//    val lampada2 = Lampada(2)
//
//
//    lampada1.turnOn()
//    repeat(3) { lampada1.cambiarColor() }
//    repeat(4) { lampada1.changeIntensity() }
//
//
//    lampada2.turnOn()
//    repeat(2) { lampada2.changeColor() }
//    repeat(4) { lampada2.changeIntensity() }
//    lampada2.turnOff()
//    lampada2.changeColor()
//    lampada2.turnOn()
//    lampada2.changeColor()
//    repeat(4) { lampada2.changeIntensity() }
//
//
//
//}
